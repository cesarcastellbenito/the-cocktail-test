export default class RoomType {
  name: string;
  hash: string;
  description: string[];
  active: boolean;
}
