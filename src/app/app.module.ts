import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import RoomTypeComponent from './shared/room/room.component';
import ImprovementsComponent from './shared/improvements/improvements.component';
import ConditionsComponent from './shared/conditions/conditions.component';
import MoreInfoComponent from './shared/moreInfo/moreInfo.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomTypeComponent,
    ImprovementsComponent,
    ConditionsComponent,
    MoreInfoComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    MatToolbarModule,
    MatExpansionModule,
    MatRadioModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
