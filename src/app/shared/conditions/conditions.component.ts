import { Component, Input } from '@angular/core';

@Component({
  selector: 'cocktail-conditions',
  templateUrl: './conditions.component.html',
  styleUrls: ['./conditions.component.scss'],
})
export default class ConditionsComponent {
  @Input() moreInfo: boolean;

}
