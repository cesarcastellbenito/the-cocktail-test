import { Component, Input, Output, EventEmitter } from '@angular/core';
import RoomType from '../../models/roomType.model';

@Component({
  selector: 'cocktail-room-type',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
})
export default class RoomTypeComponent {
  @Input() roomType: RoomType;
  @Output() onSetActive = new EventEmitter<string>();
  itemActive: string;

  selectRoom(hash) {
    this.onSetActive.emit(hash)
  }
}
