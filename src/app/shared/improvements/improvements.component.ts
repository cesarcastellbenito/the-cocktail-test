import {Component, Input} from '@angular/core';

@Component({
  selector: 'cocktail-improvements',
  templateUrl: './improvements.component.html',
  styleUrls: ['./improvements.component.scss'],
})
export default class ImprovementsComponent {
  @Input() open: boolean;
  @Input() itemsValue: any;
  @Input() title: string;

  moreInfo1: boolean = true;
  moreInfo2: boolean = false;

  openMoreInfo1() {
    this.moreInfo1 = !this.moreInfo1;
  }
  openMoreInfo2() {
    this.moreInfo2 = !this.moreInfo2;
  }

}



