import { Component, Input } from '@angular/core';

@Component({
  selector: 'cocktail-more-info',
  templateUrl: './moreInfo.component.html',
  styleUrls: ['./moreInfo.component.scss'],
})
export default class MoreInfoComponent {
  @Input() moreInfo: boolean;

  openMoreInfo() {
    this.moreInfo = !this.moreInfo;
  }
}
