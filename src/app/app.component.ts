import { Component} from '@angular/core';
import RoomType from './models/roomType.model';
import HotelName from './models/hotelName.model';
import Improve from './models/improve.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{

  activeTab: string = 'tab1';
  width: string;

  hotelName: HotelName = {
    name: 'Mieres del Camín Apartamentos'
  };
  items: RoomType[] = [
    {
      name: "Solo Alojamiento",
      hash: "bdgv45",
      description: ["Sin régimen", "Botella de agua de bienvenida"],
      active: true
    },
    {
      name: "Alojamiento y Desayuno",
      hash: "5dg8gc",
      description: ["Desayuno buffet completo"],
      active: false
    },
    {
      name: "Media Pensión",
      hash: "d4dfg8",
      description: ["Desayuno buffet completo", "Cena buffet", "No incluye"],
      active: false
    },
    {
      name: "Todo incluido",
      hash: "ty5bhb",
      description: ["Dispondrás de comida y bebida todo el día y durante toda tu estancia.", "Restaurante buffet", "Snack Bar", "Servicio de bares (marcas nacionales)"],
      active: false
    },
    {
      name: "Unlimited Services",
      hash: "3fhkir",
      description: ["Restaurante buffet con bebidas Premium", "Restaurante a la carta (una cena por estancia)", "Snack Bar con bebidas Premium", "Minibar incluido", "Acceso a SPA y 45 minutos de masaje por persona", "Actividades deportivas (acuáticas sin motor)", "Caja fuerte", "Parking gratuito", "Lavandería"],
      active: false
    }];

  improvementsTitle = "Mejora tu estancia";

  improvements: Improve[] = [
    {
      title: "Olvidese de la rutina",
      description: "Cras vehicula vestibulum dapibus. Pellentesque auctor dolor et purus facilisis maximus. Mauris eget urna luctus, imperdiet neque nec, aliquet lorem.",
      img: "../../../assets/images/meditacion.jpeg"
    },
    {
      title: "Experiencias y programas SPA con agua",
      description: "Las experiencias spa de agua tienen una presencia muy especial en SPA Sensations. No es extraño si consideramos que la mayor parte del planeta, así como del propio cuerpo humano, están compuestos de agua. El arte de recrear experiencias de agua en variaciones ilimitadas es la base de la propuesta de SPA Sensations a sus clientes, aportando el refinamiento al baño y a la hidratación en todas sus formas. Un mundo mágico de posibilidades, más allá de la terapia y del que ahora usted, también podrá disfrutar.",
      img: "../../../assets/images/spa.jpeg"
    }];
  serviceRoomTitle = "Servicios de habitación";
  serviceRoom: Improve[] = [
    {
      title: "Olvidese de la rutina",
      description: "Cras vehicula vestibulum dapibus. Pellentesque auctor dolor et purus facilisis maximus. Mauris eget urna luctus, imperdiet neque nec, aliquet lorem.",
      img: "../../../assets/images/meditacion.jpeg"
    },
    {
      title: "Pellentesque auctor dolor et purus",
      description: "Cras vehicula vestibulum dapibus. Pellentesque auctor dolor et purus facilisis maximus. Mauris eget urna luctus, imperdiet neque nec, aliquet lorem. Cras vehicula vestibulum dapibus. Pellentesque auctor dolor et purus facilisis maximus. Mauris eget urna luctus, imperdiet neque nec, aliquet lorem.",
      img: "../../../assets/images/spa.jpeg"
    }];

  openTab(tab) {
    this.activeTab = tab;

  }

  onSetActive(hash) {
    this.items = this.items.map(item => {
      item.active = item.hash === hash
      return item;
    })
  }
}
